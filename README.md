# UQ MATLAB

Files should be named after their course and contents, not module number, as modules tend to be moved around.  
As such, functions will need to be renamed so that the file name matches the function name.  
Start each script with any configuration variables, followed by a comment describing anything important.  
Also include your Slack handle (if you significantly modify someone else's file, add your Slack handle to theirs).