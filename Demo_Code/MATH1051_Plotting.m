% @bradleysigma
% for display on projector

% 200 points between -5 and 5
x = linspace(-5,5,200);
% ".^" for element-wise
y = (x+1).^2-4;
plot(x,y);
% "text(x,y,'str')" puts "str" at point (x,y)
text(-1,-4, 'min')
text(-3,0, 'root')
text(1,0, 'root')
text(0,-3, 'x-axis')