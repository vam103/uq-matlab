% @bradleysigma
% for display on projector
% should be renamed to "tuesday.m"

function y = tuesday(x)
if x < 10
    y = 1;
    disp('x less than ten');
% Not 10 <= x < 20 --> (10 <= x) < 20 --> (0 or 1) < 20 --> always true
% Use 10 <= x && x < 20
% However, x >= 10 to reach this point so also valid:
elseif x < 20
    y = 6;
    disp('x between 10 and 20');
% == for equality
elseif x == 30
    y = -17;
    disp('x is 30');
else
    y = cot(4)^2;
    disp('something else');
end
    
% Exercise Five & Six
% first100(64) will take three iterations
% first100_rev = first100(end:-1:1);
% Wrong: right = midpoint + 1; ect.