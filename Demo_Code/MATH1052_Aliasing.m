% @bradleysigma
% animates cos(12x) aliased to -cos(x/2) by 24 points

close all;

x = linspace(-2*pi,2*pi, 800);
y = cos(12*x);
for ii = 0:8:length(x)
    plot(x(1:ii), y(1:ii), 'LineWidth', 3);
    set(gcf, 'units', 'normalized', 'outerposition', [0, 0, 1, 1]);
    set(gca, 'FontSize', 18);
    title('a) Plot y=cos(12x)', 'FontSize', 48);
    xlabel('x', 'FontSize', 24);
    ylabel('y', 'FontSize', 24);
    axis([-2*pi, 2*pi, -1, 1]);
    pause(0.1-0.1*(ii/length(x))^0.5);
end

xx = linspace(-2*pi,2*pi, 24);
yy = cos(12*xx);
for ii = 1:length(xx)
    clf;
    hold on;
    plot(x, y, 'LineWidth', 3);
    plot(xx(1:ii), yy(1:ii), 'o', 'LineWidth', 3, 'MarkerSize', 18);
    set(gcf, 'units', 'normalized', 'outerposition', [0, 0, 1, 1]);
    set(gca, 'FontSize', 18);
    title('b) Sample 24 Points', 'FontSize', 48);
    xlabel('x', 'FontSize', 24);
    ylabel('y', 'FontSize', 24);
    axis([-2*pi, 2*pi, -1, 1]);
    pause(1-(ii/length(xx))^0.5);
end

for ii = 1:length(xx)
    clf;
    hold on;
    plot(x, y, 'LineWidth', 3);
    plot(xx, yy, 'o', 'LineWidth', 3, 'MarkerSize', 18);
    plot(xx(1:ii), yy(1:ii), '-', 'LineWidth', 3);
    set(gcf, 'units', 'normalized', 'outerposition', [0, 0, 1, 1]);
    set(gca, 'FontSize', 18);
    title('c) Liniearly Interpolate Samples', 'FontSize', 48);
    xlabel('x', 'FontSize', 24);
    ylabel('y', 'FontSize', 24);
    axis([-2*pi, 2*pi, -1, 1]);
    pause(1-(ii/length(xx))^0.5);
end

title('d) Realise You''ve Plotted y=-cos(x/2)', 'FontSize', 48);
pause(5);
title('e) REALISE YOU''VE PLOTTED y=-cos(x/2)!', 'FontSize', 48);