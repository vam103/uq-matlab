% @bradleysigma
% animates various saddles
% "Second Degree Saddle" is a "normal" saddle
% "Third Degrree Saddle" is a monkey saddle

[r, t] = meshgrid(0:0.01:2, linspace(0, 2*pi, 500));
x = r.*cos(t);
y = r.*sin(t);
labels = ["First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth"];
qq = [];
while 1
    for n = 1:8
        z = r.^2.*cos(n*t)/2;
        surf(x/2,y/2,z/2);
        shading interp;
%         title(labels{n} + " Degree Saddle", 'FontSize', 16, 'Position', [0, 0, 1.8]);
        axis tight;
        axis vis3d;
        box on;
        pause(1);
        [a, e] = view();
        for i = 0:5:360
            view(a+i, e);
            pause(0.05);
        end
        pause(1);
    end
end