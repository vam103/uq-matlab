% @bradleysigma
% draws three circles, using no axis, "axis square" and "axis equal"
% subplots them horizontally, and titles them appropriately

clf;
x = cos(linspace(0,2*pi,200));
y = sin(linspace(0,2*pi,200));

subplot(1,3,1);
plot(x, y, 'LineWidth', 2);
title('Default');
set(gca,'FontSize',18);

subplot(1,3,2);
plot(x, y, 'LineWidth', 2);
axis square;
title('axis square', 'FontName', 'FixedWidth');
set(gca,'FontSize',18);

subplot(1,3,3);
plot(x, y, 'LineWidth', 2);
axis equal;
title('axis equal', 'FontName', 'FixedWidth');
set(gca,'FontSize',18);