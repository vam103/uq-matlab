% step size
delta = 0.2;

% @bradleysigma
% animates euler's method for dy/dt = 1 - e^-4t - 2y
% also shows exact solution, and loops

f = @(t,y) 1-exp(-4*t)-2*y;
[T,Y] = meshgrid(-0.2:0.05:2.2, 0.2:0.05:1.2);
F = f(T,Y);
close all;
figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
quiver(T, Y, 0*F+0.02, 0.02*F, 'Color', 'b', 'LineWidth', 1, 'AutoScale', 'off');
axis equal;
axis([-0.2,2.2,0.2,1.2]);
xticks(0:delta:2);
xticklabels([]);
yticks(0.5);
yticklabels([]);
grid on;
box on;
title('$\frac{dy}{dx}=1-e^{-4t}-2y$', 'Interpreter','latex', 'FontSize', 48);
plot(-1, -1, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'x', 'MarkerSize', 12);
plot(-1, -1, 'Color', 'g', 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'none');
legend({'Gradient Field','Euler''s Method Approximation', 'Exact Solution'}, 'FontSize', 16);
n = floor(2/delta);
t = zeros(1,n+1);
y = ones(1,n+1);
for ii = 2:n+1
    t(ii) = t(ii-1)+delta;
    y(ii) = y(ii-1)+delta*f(t(ii-1), y(ii-1));
end
plot(t(1), y(1), 'Marker', 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'r', 'MarkerSize', 12, 'HandleVisibility', 'off');
while 1
    pause(10/(n-1));
    for ii = 2:n
        p_rs = plot(t(1:ii), y(1:ii), 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
        uistack(p_rs, 'bottom');
        p_rd = plot([-0.2,2.2], (y(ii)-y(ii-1))/(t(ii)-t(ii-1))*([-0.2,2.2]-t(ii-1))+y(ii-1), 'Color', 'r', 'LineStyle', ':', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
        uistack(p_rd, 'bottom');
        p_rx = plot(t(2:ii), y(2:ii), 'LineStyle', 'none', 'LineWidth', 4, 'Marker', 'x', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r', 'MarkerSize', 12, 'HandleVisibility', 'off');
        uistack(p_rx, 'bottom');
        pause(10/(n-1));
        delete([p_rs, p_rd, p_rx]);
    end
    p_rs = plot(t, y, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
    uistack(p_rs, 'bottom');
    p_rd = plot([-0.2,2.2], (y(end)-y(end-1))/(t(end)-t(end-1))*([-0.2,2.2]-t(end-1))+y(end-1), 'Color', 'r', 'LineStyle', ':', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
    uistack(p_rd, 'bottom');
    p_rx = plot(t(2:end-1), y(2:end-1), 'LineStyle', 'none', 'LineWidth', 4, 'Marker', 'x', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r', 'MarkerSize', 12, 'HandleVisibility', 'off');
    uistack(p_rx, 'bottom');
    p_rf = plot(t(end), y(end), 'LineStyle', 'none', 'Marker', 'd', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'r', 'MarkerSize', 12, 'HandleVisibility', 'off');
    pause(10/(n-1));
    delete(p_rd);
    pause(2);
    p_gs = plot(0:0.05:2, 1/2+exp(-4*(0:0.05:2))/2, 'Color', 'g', 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
    uistack(p_gs, 'down', 5);
    p_gd = plot(-0.2:0.05:2.2, 1/2+exp(-4*(-0.2:0.05:2.2))/2, 'Color', 'g', 'LineStyle', ':', 'LineWidth', 2, 'Marker', 'none', 'HandleVisibility', 'off');
    uistack(p_gd, 'down', 5);
    pause(5);
    delete([p_rs, p_rx, p_rf, p_gs, p_gd]);    
end