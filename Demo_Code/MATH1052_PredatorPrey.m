% @bradleysigma
% template for Euler's method 
% should be renamed to "predPrey.m"

function [T,X,Y] = predPrey(tmax,dt,X0,Y0, k)
steps = ceil(tmax/dt);
T = zeros(1,steps+1);
X = zeros(1,steps+1);
Y = zeros(1,steps+1);
X(1) = X0;
Y(1) = Y0;

dXdt = @(x,y) x*(1-x)-y*x/(x+0.2);
% dYdt = ...

for ii = 1:steps
    T(ii+1) = T(ii)+dt;
    X(ii+1) = X(ii)+dt*dXdt(X(ii), Y(ii));
    % Y(ii+1) = ...
end
plot(T,X,'g', T,Y,'r')
% legend, title, axes etc.
end