% @bradleysigma
% for display on projector
% should be renamed to "myProd.m"

function P = myProd(X)
P = 1; % why 1?
for ii = X % not "for ii = 1:10"
    P = P * ii;
end
end

% use i1 not il
% mySum(1:10) should not equal, say, mySum(1:20)
% (don't hardcode 1:10)
% fib(10) should not equal, say, fib(20)
% (don't hardcode n=10)
% Want to display an element of F, not F
% fib(n) should return a scalar, not a vector
% can also do "for ii = [3, 1, 4, 1, 5, 9, 2, 6]"