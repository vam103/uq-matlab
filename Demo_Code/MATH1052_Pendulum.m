% spring-pendulum, otherwise rigid
s = false;
% @bradleysigma
% pendulum animation
% can be rigid or sprung
if ~s
    l = 0.1;
    f = @(t, X) [X(2); -l*X(2)-sin(X(1))];
    [T, Y] = ode45(f, 0:0.05:20, [pi/3; 0]);
    for ii = 1:length(T)-1
        clf;
        hold on;
        plot(sin(Y(ii,1)), -cos(Y(ii,1)), 'o', 'LineWidth', 2, 'MarkerSize', 12);
        plot([0,sin(Y(ii,1))], [0,-cos(Y(ii,1))], '-', 'LineWidth', 2);
        if ii > 10
            plot(sin(Y(ii-10:ii,1)), -cos(Y(ii-10:ii,1)), '-', 'LineWidth', 2);
        else
            plot(sin(Y(1:ii,1)), -cos(Y(1:ii,1)), '-', 'LineWidth', 2);
        end
        plot(sin([pi, -pi]/3), -cos([pi, pi]/3), 'x', 'LineWidth', 2, 'MarkerSize', 12);
        axis equal;
        axis([-1,1,-1,0]);
        set(gca,'XColor', 'none','YColor','none');
        pause((T(ii+1)-T(ii))/5);
    end
else
    q = 0.2;
    f = @(t, X) [X(2); -2*X(4)*X(2)/X(3)-q/X(3)*sin(X(1)); X(4); X(3)*(X(2)^2-1)+1+q*cos(X(1))];
    [T, Y] = ode45(f, 0:0.2:50, [pi/3; 0; 1.3; 0]);
    for ii = 1:length(T)-1
        clf;
        hold on;
        plot(Y(ii,3)*sin(Y(ii,1)), -Y(ii,3)*cos(Y(ii,1)), 'o', 'LineWidth', 2, 'MarkerSize', 12);
        plot([0,Y(ii,3)*sin(Y(ii,1))], [0,-Y(ii,3)*cos(Y(ii,1))], '-', 'LineWidth', 2);
        if ii > 10
            plot(Y(ii-10:ii,3).*sin(Y(ii-10:ii,1)), -Y(ii-10:ii,3).*cos(Y(ii-10:ii,1)), '-', 'LineWidth', 2);
        else
            plot(Y(1:ii,3).*sin(Y(1:ii,1)), -Y(1:ii,3).*cos(Y(1:ii,1)), '-', 'LineWidth', 2);
        end
        axis equal;
        axis([-1.5,1.5,-2,0]);
        set(gca,'XColor', 'none','YColor','none');
        pause((T(ii+1)-T(ii))/10);
    end
end
    