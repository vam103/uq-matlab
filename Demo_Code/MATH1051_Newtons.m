% set true for oscillation
osc = false;

% @bradleysigma
% animates newton's method for of x^3-2x+2 starting from x=-1
% will advance manually for 6 iterations, then loop until broken
% with osc = true, will oscillate between x=1 and x=0

f = @(x) x.^3-2*x+2;
df = @(x) 3*x.^2-2;

ran_x = linspace(-4, 3, 200);
if osc; ran_x = linspace(-2, 2, 200); end
fun_y = f(ran_x);
x = -1;
if osc; x = 1; end
xx = [x];
loop = false;
while 1
    n = 0;
    while n <= 6 || osc
        clf;
        set(gcf, 'units', 'normalized', 'outerposition', [0, 0, 1, 1]);
        plot(ran_x, fun_y, 'LineWidth', 4);
        set(gca, 'FontSize', 24, 'LineWidth', 2, 'GridAlpha', 0.4);
        axis manual;
        xticks(x);
        xticklabels(['x_{', num2str(n), '}']);
        yticks(0);
        grid on;
        hold on;
        plot(ran_x, df(x)*(ran_x-x)+f(x), 'LineWidth', 4);
        plot(xx, f(xx), '.', 'MarkerSize', 48);
        plot(x, f(x), '.', 'MarkerSize', 72);
        x = x - f(x)/df(x);
        plot(x, 0, 'x', 'MarkerSize', 24, 'LineWidth', 4);
        xx(end+1) = x;
        if osc || loop
            pause(max(1-osc*n/100, 0.05));
        else
            pause;
        end
        n = n + 1;
    end
    if osc; continue; end
    for ii = 1:5
        title('Converged!!!!!');
        pause(0.2);
        title('');
        pause(0.2);
    end
    x = xx(1);
    xx = [x];
    n = 0;
    loop = true;
end