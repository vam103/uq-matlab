% number of steps
n = 1e3;
% probability
p = 0.5;
% number of walks
k = 5;
% frames per walk
s = 250;
% seconds per frame
w = 0.01;

% @bradleysigma
% animates k random walks, of length n, with probability p
% draws n/s steps every w seconds
% when last walk complete, deletes oldest walk and starts a new one
% figure has been know to flicker if w too small
% remedy by increasing w and decrasing s
% in a perfect world, time per walk would be w*s
% will actually be (w+a)*s for some loop overhead a

d = max([floor(n/1e3) , 1]);
t = max([floor(n/s), 1]);
v = cell(n);
m = n*(2*p-1);
s = sqrt(4*n*p*(1-p));

figure;
hold on;
axis([0, n, min([m-2*s, -s]), max([m+2*s, s])]);
fill([1:n, n, 0], [((2*p-1)*(1:n)+4*sqrt((1:n)*p*(1-p))), (2*p-1)*n, 0], [0.9, 0.9, 0.9], 'EdgeColor', 'none');
fill([1:n, n, 0], [((2*p-1)*(1:n)-4*sqrt((1:n)*p*(1-p))), (2*p-1)*n, 0], [0.9, 0.9, 0.9], 'EdgeColor', 'none');
fill([1:n, n, 0], [((2*p-1)*(1:n)+2*sqrt((1:n)*p*(1-p))), (2*p-1)*n, 0], [0.8, 0.8, 0.8], 'EdgeColor', 'none');
fill([1:n, n, 0], [((2*p-1)*(1:n)-2*sqrt((1:n)*p*(1-p))), (2*p-1)*n, 0], [0.8, 0.8, 0.8], 'EdgeColor', 'none');
plot([0,n], [0, 2*p*n-n], 'k');

c = get(gca,'colororder');

while 1
    for ii = 1:k
        c = [c(2:end,:); c(1,:)];
        y = cumsum([0, 2*(rand(1,n)<p)-1]);
        delete(v{ii});
        v{ii} = plot(0,0);
        for jj = 1:t:n
            delete(v{ii});
            v{ii} = plot(0:d:jj, y(1:d:jj+1), 'LineWidth',2, 'Color', c(1,:));
            pause(w)
        end
    end
end